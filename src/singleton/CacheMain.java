package singleton;

import singleton.Cache;

public class CacheMain {

    public static void main(String[] args) {
        Cache cache = Cache.getInstance();
        cache.put("max", Integer.MAX_VALUE);
        cache.put("min", Integer.MIN_VALUE);
        cache.put("Schueler1", "Max Mustermann");

        System.out.println(cache.get("max"));
        System.out.println(cache.get("Schueler1"));

        System.out.println(cache.get("doesn't exist"));
    }
}
