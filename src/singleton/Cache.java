package singleton;

import java.util.HashMap;
import java.util.Map;

public class Cache {

    private static Cache INSTANCE;

    private Map<String, Object> data;

    private Cache() {
        data = new HashMap<>();
    }

    public static Cache getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Cache();
        }
        return INSTANCE;
    }

    public void put(String key, Object object) {
        if (key==null){throw new IllegalArgumentException("Key must not be null");}
        this.data.put(key, object);
    }

    public Object get(String key) {
        return this.data.get(key);
    }

}
