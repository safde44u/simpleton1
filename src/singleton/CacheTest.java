package singleton;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


class CacheTest {
    Cache cache;


    @BeforeEach
    public void setup()
    {
        System.out.println("setup");
    }
    @Test
    public void testproject()
    {
        System.out.println("TestProject");
        //Arrange
        Cache cache = Cache.getInstance();
        String key = "key";
        Integer value = Integer.valueOf(10);

        //Act
        cache.put(key, value);
        Integer actualValue = (Integer) cache.get(key);

        //Assert
        assertEquals(value,actualValue);
    }
    @Test
    public void testPutNullValue()
    {
        System.out.println("TestPutNullValue");

        //Arrange
        Cache cache = Cache.getInstance();
        String key = "key";
        Integer value = Integer.valueOf(10);

        //Act
        cache.put(key, value);
        Integer actualValue = (Integer) cache.get(key);

        //Assert
        assertEquals(value,actualValue);
    }
@Test
    public void testNullKey()
    {
        System.out.println("TestNullKey");
        //Arrange
        Cache cache = Cache.getInstance();
        String key = null;
        Integer value = Integer.valueOf(10);

        try {
            //Act
            cache.put(key, value);
            fail("IllegalArgumentException should have been thrown!");
        } catch (IllegalArgumentException e)
        {
            //expected
            System.out.println(e.getMessage());
            assertEquals("Key must not be null", e.getMessage());
        }
    }
   @AfterEach
    public void teardown()
   {
       this.cache = null;
       System.out.println("teardown");
   }
   @BeforeAll
   public void setupAll()
   {
       System.out.println("setupAll");
   }
   @AfterAll
   public void teardownAll()
   {
       System.out.println("teardown All");
   }

}