package singleton;

public class MySingleton {

    private static MySingleton INSTANCE;

    private MySingleton() {
        //do nothing
    }

    public static MySingleton getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new MySingleton();
        }
        return INSTANCE;
    }

    public void doUsefulStuff() {
        System.out.println("useful stuff");
    }
}
