package singleton;

public class MySingletonMain {

    public static void main(String[] args) {
        MySingleton mySingleton = MySingleton.getInstance();

        System.out.println(mySingleton);
        // shows same object id in order to proof that we have the same instance
        System.out.println(MySingleton.getInstance());
    }
}
