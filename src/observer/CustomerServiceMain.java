package observer;

public class CustomerServiceMain {

    public static void main(String[] args) {
        CustomerService customerService = new CustomerService();
        customerService.add(new EmailService());
        customerService.add(new AddressVerificationService());
        customerService.add(new ExportData());
        customerService.save(new Customer(1, "Max Mustermann", "max.mustermann@gmail.com"));


    }
}
