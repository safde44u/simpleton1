package observer;

public class AddressVerificationService {

    public boolean checkAddress(Customer customer) {
        System.out.println("Verified address for  " + customer.getName());
        return true;
    }

}
