package observer;

public class ExportData implements CustomerStoredListener{


    private void export(Customer customer){
        System.out.println("Export customer data: " + " ID: " + customer.getId() + " Name: " + customer.getName() + " Email: " + customer.getEmail());
    }


    public void customerStored(Customer customer) {
        export(customer);
    }
}