package observer;

public class EmailService {

    public void sendEmail(Customer customer) {
        System.out.println("Sent Email to " + customer.getEmail());
    }
}
