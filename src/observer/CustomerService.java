package observer;

public class CustomerService {


    private EmailService emailService;

    private AddressVerificationService addressVerificationService;

    public CustomerService() {
        this.emailService = new EmailService();
        this.addressVerificationService = new AddressVerificationService();
    }

    public void save(Customer customer) {
        System.out.println("Successfully stored customer! " + customer.toString());
        emailService.sendEmail(customer);
        addressVerificationService.checkAddress(customer);
    }
    public void add(CustomerStoredListener customerStoredListener)
    {
        this.listeners.add(customerStoredListener);
    }
}
